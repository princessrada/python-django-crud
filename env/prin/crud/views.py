from django.shortcuts import render
from .models import Crud
from django.shortcuts import render, get_object_or_404
from .forms import DoctorForm
from django.shortcuts import redirect




# Create your views here.
def home(index):
	return render(index,'crud/home.html')

def show_doctors(request):
	docs = Crud.objects.all()

	return render(request, 'crud/show_doctors.html', {'docs' : docs})

def doctor_detail(request, pk):
    detail = get_object_or_404(Crud, pk=pk)
    return render(request, 'crud/doctor_detail.html', {'detail': detail})

def doctor_new(request):
    form = DoctorForm()
    return render(request, 'crud/doctor_create.html', {'form': form})


def doctor_new(request):
    if request.method == "POST":
        form = DoctorForm(request.POST, request.FILES)
        if form.is_valid():
 
            doctor = form.save(commit=False)
            doctor.author = request.user
            doctor.save()
            return redirect('crud.views.doctor_detail', pk=doctor.pk)
    else:
        form = DoctorForm()
    return render(request, 'crud/doctor_create.html', {'form': form})

def doctor_edit(request):
    form = DoctorForm()
    return render(request, 'crud/doctor_edit.html', {'form': form})

def doctor_edit(request, pk):
    doctor = get_object_or_404(Crud, pk=pk)
    if request.method == "POST":
        form = DoctorForm(request.POST, request.FILES, instance=doctor)
        if form.is_valid():
            doctor = form.save(commit=False)
            doctor.author = request.user
            doctor.save()
            return redirect('crud.views.doctor_detail', pk=doctor.pk)
    else:
        form = DoctorForm(instance=doctor)
    return render(request, 'crud/doctor_edit.html', {'form': form})

def doctor_delete(request):
    form = DoctorForm()
    return render(request, 'crud/doctor_delete.html', {'form': form})

def doctor_delete(request, pk):
    doctor = get_object_or_404(Crud, pk=pk)    
    if request.method=='POST':
        doctor.delete()
        return redirect('/doctor', pk=doctor.pk)
    return render(request, 'crud/doctor_delete.html', {'doctor': doctor})

# def server_delete(request, pk, template_name='servers/server_confirm_delete.html'):
#     server = get_object_or_404(Server, pk=pk)    
#     if request.method=='POST':
#         server.delete()
#         return redirect('server_list')
#     return render(request, template_name, {'object':server})