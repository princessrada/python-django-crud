from django import forms

from .models import Crud

class DoctorForm(forms.ModelForm):

    class Meta:
        model = Crud
        fields = ('title', 'specialty','text')
        