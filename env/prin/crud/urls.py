from django.conf.urls import patterns, include, url
from django.conf.urls.static import static
from django.conf import settings
from . import views


urlpatterns = patterns('',
    url(r'^$', views.home),
    url(r'^doctor/$', views.show_doctors),
    url(r'^doctor/(?P<pk>[0-9]+)/$', views.doctor_detail),
    url(r'^doctor/new/$', views.doctor_new, name='doctor_new'),
    url(r'^doctor/(?P<pk>[0-9]+)/edit/$', views.doctor_edit, name='doctor_edit'),
    url(r'^doctor/(?P<pk>[0-9]+)/delete/$', views.doctor_delete, name='doctor_delete'),


    #Auth
    url(r'^login$', 'django.contrib.auth.views.login', {'template_name': 'crud/login.html'}),
    url(r'^logout$', 'django.contrib.auth.views.logout', {'next_page': '/'}),



)

